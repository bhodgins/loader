--[[ S3 Loader :: builtin.lua - Builtin Tr0n P4ul functions
Copyright (c) 2014, S3 <s3@9600-baud.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
]]

_env['words'] = function()
   local words = {}

   for k,v in pairs(_env) do table.insert(words, k) end
   term.print(table.concat(words, " ") .. "\n")
end

_env['dup'] = function()
   local item = peek(1)
   table.insert(_S, item[1])
end

_env['*'] = function()
   local nums = get_args(2)
   table.insert(_S, nums[2] * nums[1])
end

_env['/'] = function()
   local nums = get_args(2)
   table.insert(_S, nums[2] / nums[1])
end

_env['+'] = function()
   local nums = get_args(2)
   table.insert(_S, nums[2] + nums[1])
end

_env['-'] = function()
   local nums = get_args(2)
   table.insert(_S, nums[2] - nums[1])
end

_env['.'] = function()
   if table.getn(_S) > 0 then
      term.print(table.remove(get_args(1)) .. "\n")
   end
end

_env['.S'] = function()
   term.print(table.concat(_S, " ") .. "\n")
end

_env['swap'] = function()
   local args = get_args(2)
   table.insert(_S, args[1])
   table.insert(_S, args[2])
end

_env['over'] = function()
   local args = get_args(2)
   table.insert(_S, args[2])
   table.insert(_S, args[1])
   table.insert(_S, args[2])
end

_env['drop'] = function()
   table.remove(_S)
end

_env['xor'] = function()
   local floor = math.floor
   local args = get_args(2)
   local r = 0

   for i = 0, 31 do
      local x = args[2] / 2 + args[1] / 2
      if x ~= floor (x) then
	 r = r + 2^i
      end
      args[2] = floor ( args[2] / 2)
      args[1] = floor ( args[1] / 2)
   end
   table.insert(_S, r)
end

_env['mod'] = function()
   local args = get_args(2)
   table.insert(_S, args[2] % args[1])
end

_env['abs'] = function()
   local args = get_args(1)
   table.insert(_S, math.abs(args[1]))
end

_env['-1'] = function()
   local args = get_args(1)
   table.insert(_S, args[1] - 1)
end

_env['+1'] = function()
   local args = get_args(1)
   table.insert(_S, args[1] + 1)
end

_env['2/'] = function()
   local args = get_args(1)
   table.insert(_S, args[1] / 2)
end

_env['2*'] = function()
   local args = get_args(1)
   table.insert(_S, args[1] * 2)
end

_env['rot'] = function()
   local args = get_args(3)
   table.insert(_S, args[2])
   table.insert(_S, args[1])
   table.insert(_S, args[3]) 
end

_env['-rot'] = function()
   local args = get_args(3)
   table.insert(_S, args[1])
   table.insert(_S, args[3])
   table.insert(_S, args[2])
end

_env['nip'] = function()
   _env['swap']()
   _env['drop']()
end

_env['tuck'] = function()
   _env['swap']()
   _env['over']()
end

_env['yield'] = function()
   local event = {coroutine.yield()}
   for i=#event, 1, -1 do
      table.insert(_S, event[i])
   end
end

_env['fyield'] = function()
   local _filter = get_args(1)[1]
   local event = {coroutine.yield(_filter)}
   for i=#event, 2, -1 do
      table.insert(_S, event[i])
   end
end

_env['help'] = function()
   term.print([=[Summary of basic commands:
/path/to/boot_file image - sets boot image
arg1 arg2 ... argn boot - boot specified image
words - displays advanced command list

]=])
end

_env['showstack'] = function() _show_stack = true  end
_env['hidestack'] = function() _show_stack = false end

-- Compiling functions:
_env[':'] = function()
   -- Function never gets added until ;
   _nop = ";"
   _RSP = -1
end

_env[';'] = function()
   local args  = get_args(_RSP)
   local fname = table.remove(args)
   local func  = {}
   for i=#args, 1, -1 do
      table.insert(func, args[i])
   end
   _env[fname] = func
   _RSP = nil
end

_env['image'] = function()
   local image = get_args(1)[1]
   boot_image = image
end

-- [" STRING CONCATENATION "]:
_env['["'] = function()
   _nop = '"]'
   _RSP = -g
end

_env['"]'] = function()
   local args = get_args(_RSP)
   local str = ""
   _RSP = nil

   for i=#args, 1, -1 do
      str = str .. " " .. args[i]
   end

   table.insert(_S, str)
end

_env['('] = function()
   _nop = ")"
   _RSP = -1
end

_env[')'] = function()
   local args = get_args(_RSP)
end

-- Anonumous functions:
_env['['] = function()
   _nop = "]"
   _RSP = -1
end

_env[']'] = function()
   local func = {}
   local args = get_args(_RSP)
   for i=_RSP, 1, -1 do
      table.insert(func, args[i])
   end
   table.insert(_S, func)
end

_env[':'] = function()
   _nop = ':'
end

_env['boot'] = function()
   local params = get_args(1)[1]
   local err
   
   if boot_image == "" then
      term.print("No image specified\n")
      return false
   end
   
   -- Make a copy of _G:
   local u = {}
   local params = {} -- Not used yet
   for k, v in pairs(_G) do u[k] = v end
   local new_env = setmetatable(u, getmetatable(_G))
   
   -- Load the image into a Lua chunk:
   local file = fs.open(boot_image, "r")
   if file then
      local func, err = loadstring(file.readAll(), fs.getName(boot_image))
      file.close()
      
      if err and err ~= "" then
	 term.print("Error: " .. err .. "\n")
	 return false
      end
      
      local env = new_env
      setfenv( f_file, env)
      local success, err = pcall(function() func(unpack(params)) end)
      if not success then
	 if err and err ~= "" then
	    term.print("Error: " .. err .. "\n")
	 end
	 return false
      end
      return false
   end   
end
