#/bin/sh

OUT_FILE="bios.lua"

# Order is critical:
declare -a SRCFILES=(
'term.lua'
'stackops.lua'
'builtin.lua'
'repl.lua'
'main.lua'
)

declare -A DOCUMENTS=(
['motd.txt']='MOTD'
['default.tp']='default_conf'
)

# Preliminary:
if [ -f ${OUT_FILE} ]; then rm ${OUT_FILE}; fi
echo -e "--[[" $(cat license.txt) "\n]]" > ${OUT_FILE}
more +/"]]" globals.lua | sed '1,5d' >> ${OUT_FILE}
echo >> ${OUT_FILE} # \n

# Generate heredocs:
for document in "${!DOCUMENTS[@]}"
do
    echo -n "local" ${DOCUMENTS["$document"]} "= [=[" >> ${OUT_FILE}
    cat ${document} >> ${OUT_FILE}
    echo -e "]=]\n" >> ${OUT_FILE}
    echo ${document}
done

# Concatenate sources:
for file in ${SRCFILES[@]}
do
    more +/"]]" ${file} | sed '1,5d' >> ${OUT_FILE}
    echo -e "\n" >> ${OUT_FILE}
    echo ${file}
done
