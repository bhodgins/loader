--[[ S3 Loader :: main.lua - Entry point
Copyright (c) 2014, S3 <s3@9600-baud.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
]]

if _debug then
   -- No term? must be running outside of CC:
   print("\n<<<<<<<<<<<< DEBUG MODE >>>>>>>>>>>>\n")
   term.print = function(str) io.write(str) end
   term.read  = function(prompt)
      io.write(prompt .. " ")
      local  input = io.read('*l')
      return input
   end
   
   os.startTimer = function() end
   coroutine.yield = function() end
   print(MOTD)
   repl()
end

term.print(MOTD)
os.startTimer(2)
local event = {coroutine.yield()}
if event[1] == "timer" then
   if _timeout == true then
      if boot_image ~= "" then
	 _env['boot']()
      end
   end
   term.print("No default image. dropping to shell:\n")
   repl()
elseif event[1] == "key" or event[1] == "char" then
   _timeout = false
   repl()
end
