--[[ S3 Loader :: repl.lua - Read Eval Print Loop
Copyright (c) 2014, S3 <s3@9600-baud.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
]]

function eval(str)
   local success = 1
   for word in str:gmatch('%S+') do
      if _nop == "" then
	 if _env[word] then
	    if type(_env[word]) == "function" then _env[word]() -- Builtin
	    elseif type(_env[word]) == "table" then
	       -- User defined function:
	       eval(table.concat(_env[word], " "))
	    end
	 else table.insert(_S, word) end -- BUG
      else
	 if _nop == word then
	    _nop = ""
	    _env[word]()
	 else
	    table.insert(_S, word)
	 end
      end

      -- Handle the relative stack pointer for Spec Ops:
      if _RSP == nil then
	 --[[ BUG? if not _RSP == nil does NOT work. SAD FACE :(
	      Be retarded and do nothing. ]]--
      else _RSP = _RSP + 1 end -- :)

   end
   return success
end

function repl()
   while true do
      local input = term.read("%")
      if input then
	 if eval(input) then
	    if _show_stack == true then
	       term.print("=> [")
	       for i=1, #_S do
		  if type(_S[i]) ~= "table" then
		     term.print(_S[i])
		  else term.print("LAMBDA") end
		  if i < #_S then term.print(", ") end
	       end
	       term.print("]\n")
	    end
	 end
      elseif input == "quit" then
	 return false
      end
   end
end
