--[[ S3 Loader :: term.lua - Additional terminal handling functions
Copyright (c) 2014, S3 <s3@9600-baud.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
]]

if not term then
   term = {}
   _debug = true
   _show_stack = true
else
   term_x, term_y = term.getSize()
end

function backspace(limit_x, limit_y)
   if cur_x > 1 and cur_y >= 1 then
      if cur_x == limit_x and cur_y == limit_y then
	 return false
      else
	 if cur_x > 1 then
	    cur_x = cur_x - 1
	    term.setCursorPos(cur_x, cur_y)
	    term.write(" ")
	    term.setCursorPos(cur_x, cur_y)
	    return true
	 else
	    cur_x = term_x
	    cur_y = cur_y - 1
	    term.setCursorPos(cur_x, cur_y)
	    term.write(" ")
	    term.setCursorPos(cur_x, cur_y)
	    return true
	 end
      end
   end
end

if not _debug then
   term.print = function(str)
      str = tostring(str)
      cur_x, cur_y = term.getCursorPos()
      for i=1,#str do
	 if cur_x == term_x then
	    cur_x = 0
	    if cur_y < term_y then
	       cur_y = cur_y + 1
	    else
	       term.scroll(1)
	    end
	 end
	 local char = str:sub(i,i)
	 if char == "\n" then
	    cur_x = 1         -- \r
	    if cur_y < term_y then
	       cur_y = cur_y + 1 -- \n
	    else
	       term.scroll(1)
	    end
	 else
	    term.write(char)
	    cur_x = cur_x + 1
	 end
	 term.setCursorPos(cur_x, cur_y)
      end
   end
   
   term.read = function(prompt)
      local start_x = cur_x + (#prompt + 1)
      local start_y = cur_y
      local input = ""
      term.print(prompt .. " ")
      term.setCursorBlink(true)

      repeat
	 local event = {coroutine.yield()}
	 
	 if event[1] == "char" then
	    term.print(event[2])
	    input = input .. event[2]
	 elseif event[1] == "key" then
	    if event[2] == 28 then
	       term.print("\n")
	       input = input .. "\n" -- normal term behavior
	    elseif event[2] == 14 then
	       if backspace(start_x, start_y) == true then
		  input = input:sub(1, (#input - 1))
	       end
	    end
	 end
      until event[2] == 28

      return input
   end
end
